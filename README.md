# apu_pcengines_hal

[![crates.io](https://img.shields.io/crates/v/apu_pcengines_hal?logo=rust)](https://crates.io/crates/apu_pcengines_hal/)
[![CI pipeline](https://gitlab.com/dns2utf8/apu_pcengines_hal/badges/master/pipeline.svg)](https://gitlab.com/dns2utf8/apu_pcengines_hal/)

A safe wrapper around the direct memory interface of the APU2+ hardware.

## Installation

### With cargo (or rustup) from git

```bash
sudo apt install build-essential
git clone https://gitlab.com/dns2utf8/apu_pcengines_hal.git
cargo build --release --example leds
sudo ./target/release/examples/leds
```

### Download nightly from gitlab:

The CI builds the binary on the `main` branch on every commit.
You can download the **leds example** program from [https://dns2utf8.gitlab.io/apu_pcengines_hal/leds](https://dns2utf8.gitlab.io/apu_pcengines_hal/leds) or use these commands:

```bash
wget https://dns2utf8.gitlab.io/apu_pcengines_hal/leds
sudo ./leds
```
