use apu_pcengines_hal::{MappingResult, APU_LED1, APU_LED2, APU_LED3};
use std::thread::sleep;
use std::time::Duration;

fn main() -> MappingResult<()> {
    const ONE_SEC: Duration = Duration::from_secs(1);
    let mut map = apu_pcengines_hal::init()?;

    println!("init done: {:?}", map);

    {
        let led2 = map.get_pin(APU_LED2)?;
        println!("led2: {:?}", led2);

        let mut led2 = led2.into_input();
        let val = led2.get();
        println!("led2: {:?} = {}", led2, val);

        let mut led2 = led2.into_output();
        println!("led2: {:?}", led2);
        led2.set(!val);

        map.free_pin(led2);
    }
    {
        let led2 = map.set_output(APU_LED2)?;
        println!("led2: {:?}", led2);
        map.free_pin(led2);
    }
    {
        let led2 = map.get_pin(APU_LED2)?;
        println!("led2: {:?}", led2);
        map.free_pin(led2);
    }

    let led1 = map.set_output(APU_LED1)?;
    println!("led1: {:?}", led1);
    let mut led2 = map.set_output(APU_LED2)?;
    println!("led2: {:?}", led2);
    let mut led3 = map.set_output(APU_LED3)?;
    println!("led3: {:?}", led3);

    let mut val = true;

    loop {
        print!("{}", if val { "t" } else { "f" });
        flush_stdout().unwrap();
        led2.set(val);
        val = !val;
        led3.set(val);
        print!(".");
        flush_stdout().unwrap();
        sleep(ONE_SEC);
    }
}

fn flush_stdout() -> std::io::Result<()> {
    use std::io::Write;
    std::io::stdout().flush()?;
    std::io::stderr().flush()
}
